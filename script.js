document.addEventListener('DOMContentLoaded', function() {
   const tabs = document.querySelector('.tabs');
   const tabContents = document.querySelectorAll('.tab-content');

   tabs.addEventListener('click', function(event) {
       const target = event.target;
       if (target.classList.contains('tabs-title')) {
           const activeTab = document.querySelector('.tabs-title.active');
           if (activeTab) {
               activeTab.classList.remove('active');
           }
           target.classList.add('active');

           const targetTab = target.getAttribute('data-tab');

           tabContents.forEach(content => {
               if (content.getAttribute('data-tab') === targetTab) {
                   content.classList.add('active');
               } else {
                   content.classList.remove('active');
               }
           });
       }
   });
});
